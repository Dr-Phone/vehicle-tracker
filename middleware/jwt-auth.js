import { verify } from "jsonwebtoken";

export default function authenticate(req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = verify(token, process.env.JWT_SECRET_KEY);
    req.decodedUserData = decoded;
  } catch (error) {
    return res.status(401).json({
      message: "Authentication failed!",
    });
  }
  next();
}
