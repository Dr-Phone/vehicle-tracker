import { ForbiddenException } from "../errors";
import * as vehiclesService from "../services/vehicles.service";

export const registerVehicle = async (req, res, next) => {
  console.log("Processing func -> registerVehicle()");
  console.log("req-body:", req.body);
  try {
    if (req.decodedUserData.name !== req.body.driverName) {
      throw new ForbiddenException(
        "Cannot register vehicle on some other user"
      );
    }
    const vehicle = await vehiclesService.registerVehicle(
      req.body.vehicleId,
      req.body.type,
      req.body.driverName,
      req.body.make
    );
    res.status(201).json({
      message: "Vehicle registered",
      vehicle
    });
  } catch (err) {
    console.log("registerVehicle():", err);
    next(err);
  }
};

export async function getAllVehicles(req, res, next) {
  try {
    const vehicles = await vehiclesService.getAllVehicles();
    res.status(200).json({
      vehicles,
    });
  } catch (err) {
    console.log("getAllVehicles():", err);
    next(err);
  }
};
