import { ForbiddenException } from "../errors";
import * as tripsService from "../services/trips.service";

export const startTrip = async (req, res, next) => {
  console.log("Processing func -> startTrip()");
  try {
    if (req.decodedUserData.name !== req.body.driverName) {
      throw new ForbiddenException("Cannot start trip for some other driver");
    }
    const trip = await tripsService.startTrip(
      req.body.lat,
      req.body.long,
      req.body.driverName,
      req.body.vehicleId
    );
    res.status(201).json({
      message: "Trip started",
      trip,
    });
  } catch (err) {
    console.log("startTrip():", err);
    next(err);
  }
};

export const saveTripProgress = async (req, res, next) => {
  console.log("Processing func -> updateOngoingTrip()");

  try {
    const progress = await tripsService.saveTripProgress(
      req.params.id,
      req.body.lat,
      req.body.long
    );
    res.status(201).json({
      message: "Trip progress saved!",
      progress,
    });
  } catch (err) {
    console.log("saveTripProgress():", err);
    next(err);
  }
};

export const endTrip = async (req, res, next) => {
  console.log("Processing func -> endTrip()");

  try {
    const trip = await tripsService.endTrip(
      req.params.id,
      req.body.lat,
      req.body.long
    );
    res.status(201).json({
      message: "Trip ended",
      trip,
    });
  } catch (err) {
    console.log("endTrip():", err);
    next(err);
  }
};

export const saveStats = async (req, res, next) => {
  console.log("Processing func -> saveStats()");
  try {
    const trip = await tripsService.saveStats(
      req.params.id,
      req.body.metricOne,
      req.body.metricTwo,
      req.body.metricThree
    );
    res.status(201).json({
      message: "Trip stats saved",
      trip,
    });
  } catch (err) {
    console.log("saveStats():", err);
    next(err);
  }
};

export async function getAllTrips(req, res, next) {
  try {
    const trips = await tripsService.getAllTrips();
    res.status(200).json({
      trips,
    });
  } catch (err) {
    console.log("getAllTrips():", err);
    next(err);
  }
};

export async function getOngoingTrips(req, res, next) {
  try {
    const trips = await tripsService.getOngoingTrips();
    res.status(200).json({
      trips,
    });
  } catch (err) {
    console.log("getOngoingTrips():", err);
    next(err);
  }
}
