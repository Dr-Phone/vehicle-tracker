import * as driversService from "../services/drivers.service";

export async function registerDriver(req, res, next) {
  console.log("Processing func -> registerVehicle()");
  console.log("req-body:", req.body);
  try {
    let driver = await driversService.registerDriver(
      req.body.name,
      req.body.password,
      req.body.dob
    );

    res.status(201).json({
      message: "Sign Up successful",
    });
  } catch (err) {
    console.log(`registerDriver(): ${err}`);
    next(err);
  }
}

export async function signInDriver(req, res, next) {
  try {
    const validDriver = await driversService.validateDriver(
      req.body.name,
      req.body.password
    );
    if (validDriver) {
      res.status(200).json({
        message: "Sign In successful",
        token: driversService.getJwtToken(req.body.name),
      });
    } else {
      res.status(401).json({
        message: "Sign In Failed",
      });
    }
  } catch (err) {
    console.log(`signInDriver: ${err}`);
    next(err);
  }
}

export async function getAllDrivers(req, res, next) {
  try {
    let allDrivers = await driversService.getAllDrivers();
    let drivers = [];
    allDrivers.forEach((driver) => {
      drivers.push(driver.name);
    });
    res.status(200).json({
      drivers: drivers,
    });
  } catch (err) {
    console.log(`getAllDrivers: ${err}`);
    next(err);
  }
}
