import { Router } from "express";
import {
  startTrip,
  endTrip,
  saveStats,
  saveTripProgress,
  getAllTrips,
  getOngoingTrips
} from "../controllers/trips.controller";
import authenticate from "../middleware/jwt-auth";
const router = Router();

/* GET trips listing. */
router.get("/", function (req, res, next) {
  getAllTrips(req, res, next);
});

router.get("/ongoing", function (req, res, next) {
  getOngoingTrips(req, res, next);
});

// create a new trip
router.post("/", authenticate, function (req, res, next) {
  console.log("Creating a new trip!");
  startTrip(req, res, next);
});

// update an ongoing trip
router.post("/:id/update", authenticate, function (req, res, next) {
  saveTripProgress(req, res, next);
});

// end an ongoing trip
router.post("/:id/end", authenticate, function (req, res, next) {
  console.log("end an ongoing trip with id");
  endTrip(req, res, next);
});

// save stats for an ended trip
router.post("/:id/stats", authenticate, function (req, res, next) {
  console.log("saving stats for an ended trip with id");
  saveStats(req, res, next);
});

export default router;

/*
  GET trips     /trips
  POST create   /trips
  POST edit   /trips/:id/edit
  PUT update  /trips/:id
*/
