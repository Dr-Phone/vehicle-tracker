import { Router } from "express";
var router = Router();

const API_REF = ` Welcome to Vehicle Tracking. Api ref-

1. Register driver

POST http://13.126.49.87:3000/drivers/signup
Body {
	name: Aman
	password: tooWeak
}


1b. Get all drivers

GET http://13.126.49.87:3000/drivers/


2. Driver Sign In

POST http://13.126.49.87:3000/drivers/signin
Body {
	name: Aman
	password: tooWeak
}


3. Register vehicle - POST, Get all vehicles - GET

POST http://13.126.49.87:3000/vehicles/
Header {
	Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQW1hbiIsImlhdCI6MTYwMTkwNzY3MCwiZXhwIjoxNjAxOTk0MDcwfQ.HQJgJRb2kl3Ke6yxuQX3bnK7prQ_nZ_WZjb6trndo28
}
Body {
	vehicleId:	KA150000
	type:	SUV
	driverName: Aman
}


4. Start trip - POST, Get all trips - GET

POST http://13.126.49.87:3000/trips
Body {
	lat: 12.972442
	long: 77.580643
	driverName: Aman
	vehicleId:	KA150000
}


5. Update trip for tracking

POST http://13.126.49.87:3000/trips/1/update   (endpoint uses trip id param)
Body {
	lat: 12.9642
	long: 77.5807
}

POST http://13.126.49.87:3000/trips/1/update   
Body {
	lat: 12.967
	long: 77.5806
}


POST http://13.126.49.87:3000/trips/1/update   (endpoint uses trip id param)
Body {
	lat: 12.9673
	long: 77.58061
}

...


6. End trip

POST http://13.126.49.87:3000/trips/2/end   (endpoint uses trip id param)
Body {
	lat: 12.98
	long: 77.59
}


7. Save trip stats

POST http://13.126.49.87:3000/trips/1/stats   (endpoint uses trip id param)
Body {
	metricOne: 1
	metricTwo: 2
	metricThree: 3
}`;

/* GET home page. */
router.get("/", function (req, res, next) {
  res.send(API_REF);
});

export default router;
