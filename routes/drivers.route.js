import { Router } from "express";
import {
  getAllDrivers,
  registerDriver,
  signInDriver,
} from "../controllers/drivers.controller";

var router = Router();

/* GET drivers listing. */
router.get("/", function (req, res, next) {
  getAllDrivers(req, res, next);
});

// create driver
router.post("/signup", function (req, res, next) {
  registerDriver(req, res, next);
});

router.post("/signin", function (req, res, next) {
  signInDriver(req, res, next);
});

export default router;
