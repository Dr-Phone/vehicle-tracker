import { Router } from "express";
import {
  registerVehicle,
  getAllVehicles,
} from "../controllers/vehicles.controller";
import authenticate from "../middleware/jwt-auth";
const router = Router();

/* GET vehicles listing. */
router.get("/", function (req, res, next) {
  getAllVehicles(req, res, next);
});

// create vehicle
router.post("/", authenticate, function (req, res, next) {
  console.log("creating vehicle!");
  registerVehicle(req, res, next);
});

export default router;
