export class ConflictException extends Error {
  constructor(message) {
    super(message);
    this.status = 409;
    this.name = "ConflictException";
    Error.captureStackTrace(this, ConflictException);
  }
}

export class ForbiddenException extends Error {
  constructor(message) {
    super(message);
    this.status = 403;
    this.name = "ForbiddenException";
    Error.captureStackTrace(this, ForbiddenException);
  }
}

export class NotFoundException extends Error {
  constructor(message) {
    super(message);
    this.status = 404;
    this.name = "NotFoundException";
    Error.captureStackTrace(this, NotFoundException);
  }
}

export class BadRequestException extends Error {
  constructor(message) {
    super(message);
    this.status = 400;
    this.name = "BadRequestException";
    Error.captureStackTrace(this, BadRequestException);
  }
}