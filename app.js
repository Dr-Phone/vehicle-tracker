"use strict";

import express, { json, urlencoded } from "express";
import logger from "morgan";

import indexRouter from "./routes/index.route";
import tripsRouter from "./routes/trips.route";
import driversRouter from "./routes/drivers.route";
import vehiclesRouter from "./routes/vehicles.route";
var cors = require("cors");

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(json());
app.use(urlencoded({ extended: false }));

app.use("/", indexRouter);
app.use("/trips", tripsRouter);
app.use("/drivers", driversRouter);
app.use("/vehicles", vehiclesRouter);

// all routes will eventually hit this by default if response is not sent
// or if it doesn't hit a route
app.use("*", function (req, res, next) {
  const error = new Error("Not found!");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error,
    message: error.message,
  });
});

export default app;
