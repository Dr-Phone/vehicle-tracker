import { driver as DriverModel } from "../models";
import { genSalt, hashSync, compareSync } from "bcryptjs";
import { ConflictException, BadRequestException } from "../errors";
import DriverMongo from "../models/drivers-mongo.model";
const jwt = require("jsonwebtoken");

try {
  require("dotenv").config();
} catch (err) {
  console.log(err);
}

export async function registerDriver(name, password, dob) {
  let driver = await getDriver(name);
  if (!dob) {
    throw new BadRequestException("dob is required!");
  }
  if (driver) {
    throw new ConflictException("Driver with the given name already exists!");
  }
  const salt = await genSalt(2);
  driver = await DriverModel.create({
    name: name,
    password: hashSync(password, salt),
    salt,
  });
  let driverMongo = await DriverMongo.create({
    _id: name,
    dob: dob
  })
  return driver;
}

export async function getDriver(name) {
  let driver = await DriverModel.findOne({
    where: {
      name: name,
    },
  });
  return driver;
}

export async function validateDriver(name, password) {
  const driver = await getDriver(name);
  if (!driver) {
    return false;
  } else {
    return compareSync(password, driver.password);
  }
}

export async function getAllDrivers() {
  return DriverModel.findAll();
}

export function getJwtToken(driverName) {
  return jwt.sign({ name: driverName }, process.env.JWT_SECRET_KEY, {
    expiresIn: "1d",
  });
}
