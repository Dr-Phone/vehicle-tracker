import { ConflictException, BadRequestException } from "../errors";
import { vehicle as VehicleModel } from "../models";
import VehicleMongo from "../models/vehicles-mongo.model";
import { getDriver } from "./drivers.service";

export async function registerVehicle(vehicleId, vehicleType, driverName, make) {
  const driver = await getDriver(driverName);
  let vehicle = await getVehicle(vehicleId);
  if (!make) {
    throw new BadRequestException("make is required!");
  }
  if (vehicle) {
    throw new ConflictException("This vehicle is already registered!");
  }
  vehicle = await VehicleModel.create({
    id: vehicleId,
    type: vehicleType,
    driverName: driver.name,
  });
  
  let vehicleMongo = await VehicleMongo.create({
    _id: vehicleId,
    make: make
  });
  return vehicle;
}

export async function getVehicle(vehicleId) {
  let vehicle = await VehicleModel.findOne({
    where: {
      id: vehicleId,
    },
  });
  return vehicle;
}

export async function getAllVehicles() {
  return VehicleModel.findAll();
}
