import { trip as TripModel, tripStats as TripStatsModel } from "../models";
import { getDriver } from "./drivers.service";
import { getVehicle } from "./vehicles.service";
import TripProgress from "../models/trips-progress";
import {
  ConflictException,
  ForbiddenException,
  NotFoundException,
} from "../errors";
import TripMongo from "../models/trips-mongo.model";

export async function startTrip(lat, long, driverName, vehicleId) {
  const driver = await getDriver(driverName);
  const vehicle = await getVehicle(vehicleId);

  if (!driver) {
    throw new NotFoundException("Driver not found");
  }

  if (!vehicle) {
    throw new NotFoundException("Vehicle not found");
  }

  if (vehicle.driverName !== driver.name) {
    throw new ForbiddenException("Vehicle not registered with this driver!");
  }

  // see that this driver is not already on an ongoing trip
  // No need to check with vehicle because of the assumption that each vehicle can be associated with only one driver.
  let trip = await getOngoingTripByDriver(driverName);
  if (trip) {
    throw new ConflictException(
      "Driver cannot create new trip without ending the current ongoing trip!"
    );
  }

  // TODO: These two should come in one unit of work. Use transactions
  {
    // 1
    trip = await TripModel.create({
      startTime: new Date(),
      startLocationLat: lat,
      startLocationLong: long,
      driverName: driver.name,
      vehicleId: vehicle.id,
    });

    let tripMongo = await TripMongo.create({
      _id: trip.id,
      startTime: trip.startTime,
      driverName: driver.name,
      vehicleId: vehicle.id,
    });

    // 2
    await saveTripProgress(trip.id, lat, long, trip.startTime);
  }
  return trip;
}

export async function getTripById(tripId) {
  const trip = await TripModel.findOne({
    where: {
      id: tripId,
    },
  });
  return trip;
}

export async function getOngoingTripByDriver(driverName) {
  const trip = await TripModel.findOne({
    where: {
      driverName: driverName,
      endTime: null,
    },
  });
  return trip;
}

export async function getOngoingTripByVehicle(vehicleId) {
  const trip = await TripModel.findOne({
    where: {
      vehicleId: vehicleId,
      endTime: null,
    },
  });
  return trip;
}

export async function saveTripProgress(tripId, lat, long, timestamp = null) {
  let trip = await getTripById(tripId);

  if (trip && trip.endTime) {
    throw new ConflictException("Trip has already been ended!");
  }

  let progress = await TripProgress.create({
    tripId: tripId,
    timestamp: timestamp ? timestamp : new Date(),
    location: {
      type: "Point",
      coordinates: [long, lat],
    },
  });

  return progress;
}

export async function endTrip(tripId, lat, long) {
  let trip = await getTripById(tripId);

  if (trip && trip.endTime) {
    console.log("trip:", trip);
    throw new ConflictException("Trip has already been ended!");
  }

  const endTime = new Date();

  await saveTripProgress(trip.id, lat, long, trip.endTime);

  trip.endTime = endTime;
  trip.endLocationLat = lat;
  trip.endLocationLong = long;

  trip = await trip.save();

  let tripMongo = await TripMongo.findByIdAndUpdate(trip.id, { endTime: endTime })
  
  return trip;
}

export async function saveStats(tripId, metricOne, metricTwo, metricThree) {
  let trip = await getTripById(tripId);
  let stat = await getTripStats(trip.id);
  if (stat) {
    throw new ConflictException("stats already saved for this trip!");
  }
  if (trip && trip.endTime) {
    const tripStats = TripStatsModel.create({
      tripId: trip.id,
      metricOne,
      metricTwo,
      metricThree,
    });
    return tripStats;
  } else {
    throw new ConflictException("cannot save stats for an ongoing trip!");
  }
}

export async function getTripStats(tripId) {
  const stat = await TripStatsModel.findOne({
    where: {
      tripId: tripId,
    },
  });
  return stat;
}

export async function getAllTrips() {
  return TripModel.findAll();
}

export async function getOngoingTrips() {
  const trips = await TripMongo.find({ endTime: null })
    .populate("driverName")
    .populate("vehicleId");
  console.log('trips:\n', trips);
  return trips;
}