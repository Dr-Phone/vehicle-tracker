import { Schema, model } from "mongoose";

const tripMongoSchema = new Schema({
    _id: {
      type: Number,
      required: true,
    },
    startTime: {
      type: Date,
      required: true,
    },
    endTime: {
      type: Date,
      required: false,
    },
    driverName: {
      type: String,
      ref: "DriverMongo"
    },
    vehicleId: {
      type: String,
      ref: "VehicleMongo"
  }
});

export default model("TripMongo", tripMongoSchema);
