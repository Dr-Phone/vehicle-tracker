export default (sequelize, DataTypes) => {
  const Trip = sequelize.define("trip", {
    // auto generated trip id
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    startTime: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    startLocationLat: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    startLocationLong: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    endTime: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    endLocationLat: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    endLocationLong: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
  });
  return Trip;
};
