import { Schema, model } from "mongoose";

const vehicleMongoSchema = new Schema({
    _id: {
        type: String,
        required: true   
    },
    make: {
        type: String,
        required: true,
    },
});

export default model("VehicleMongo", vehicleMongoSchema);
