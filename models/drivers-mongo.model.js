import { Schema, model } from "mongoose";

const driverMongoSchema = new Schema({
    _id: {
        type: String,
        required: true,
    },
    dob: {
        type: Date,
        required: true,
    },
});

export default model("DriverMongo", driverMongoSchema);
