export default (sequelize, DataTypes) => {
  const Vehicle = sequelize.define("vehicle", {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
      validate: {
        min: 6,
        max: 10,
      },
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 3,
        max: 15,
      },
    },
  });
  return Vehicle;
};
