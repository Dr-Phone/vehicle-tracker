"use strict";

const Sequelize = require("sequelize");

try {
  require("dotenv").config();
} catch (err) {
  console.log(err);
}

const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "/../config/config.json")[env];
const mongoose = require("mongoose");

const mongoDbName =
  env === "development" ? "vehicleTracker" : "vehicle_tracker_production";
const mongoPassword = process.env.MONGO_PASSWORD || "";
const mongoURL =
  env === "development"
    ? `mongodb://localhost:27017/${mongoDbName}`
    : `mongodb+srv://developer:${mongoPassword}@cluster0.twlem.mongodb.net/${mongoDbName}?retryWrites=true&w=majority`;

mongoose.connect(mongoURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const mongo = mongoose.connection;
mongo.on("error", console.error.bind(console, "connection error:"));
mongo.once("open", function () {
  console.log(`Mongoose is connected on ${env} env!`);
});

const db = {};

let sequelize;
if (env === "development") {
  if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
  } else {
    sequelize = new Sequelize(
      config.database,
      config.username,
      config.password,
      config
    );
  }
} else {
  const mysqlDbName = process.env.MYSQL_DB_NAME;
  const mysqlUsername = process.env.MYSQL_USER;
  const mysqlPassword = process.env.MYSQL_PASSWORD;

  sequelize = new Sequelize(mysqlDbName, mysqlUsername, mysqlPassword, {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  });
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.mongoose = mongoose;

db.driver = require("./drivers.model.js").default(sequelize, Sequelize);
db.vehicle = require("./vehicles.model.js").default(sequelize, Sequelize);
db.trip = require("./trips.model.js").default(sequelize, Sequelize);
db.tripStats = require("./trip-stats.model.js").default(sequelize, Sequelize);

db.vehicle.belongsTo(db.driver);
db.trip.belongsTo(db.driver);
db.trip.belongsTo(db.vehicle);
db.tripStats.belongsTo(db.trip);

module.exports = db;
