export default (sequelize, DataTypes) => {
  const Driver = sequelize.define("driver", {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      validate: {
        min: 3,
        max: 15,
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        min: 6,
        max: 15,
      },
    },
    salt: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
  return Driver;
};
