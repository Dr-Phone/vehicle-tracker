import { Schema, model } from "mongoose";

const pointSchema = new Schema({
  type: {
    type: String,
    enum: ["Point"],
    required: true,
  },
  coordinates: {
    type: [Number],
    required: true,
  },
});

const tripProgressSchema = new Schema({
  tripId: Number,
  timestamp: Date,
  location: {
    type: pointSchema,
    required: true,
  },
});

export default model("TripProgress", tripProgressSchema);
